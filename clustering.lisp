;;; ===================================
;;; Rodas de Paz, Alejandro
;;; Inteligencia Artificial 2
;;; ===================================

(defun max-cons (a b)
  "Función auxiliar para maximizar una reducción"
  (if (> (car a) (car b)) a b))

(defun min-cons (a b)
  "Función auxiliar para minimizar una reducción.
   Permite comparaciones con NIL"
  (let ((v-a (car a))
	(v-b (car b)))
    (cond ((null v-a) b)
	  ((null v-b) a)
	  ((< v-a v-b) a)
	  (T b))))

(defun mostrar-seleccion-incorrecta (func)
  (format t "Selección incorrecta~%")
  (funcall func))

(defparameter datos-ejemplo (make-array 5 :initial-contents '((1.0) (4.0) (5.0) (9.0) (11.0))))


;;; ======== Distancias ========

(defun distancia-euclidea (x y)
  (sqrt (loop for xi in x for yi in y
	   summing (expt (- xi yi) 2))))

(defun distancia-manhattan (x y)
  (loop for xi in x for yi in y
     summing (abs (- xi yi))))

(defparameter *distancia* #'distancia-euclidea)

(defun seleccionar-distancia ()
  (format t "Seleccione una distancia:~%")
  (format t "1. Distancia Euclídea~%")
  (format t "2. Distancia Manhattan~%")
  (let ((input (read)))
    (setf *distancia*
	(cond ((eq 1 input) #'distancia-euclidea)
	      ((eq 2 input) #'distancia-manhattan)
	      (t (mostrar-seleccion-incorrecta #'seleccionar-distancia))))))

(defun distancia (x y)
  (funcall *distancia* x y))


;;; ======== Linkages ========

(defun single-linkage (i j l distancias)
  (min (aref distancias l i) (aref distancias l j)))

(defun complete-linkage (i j l distancias)
  (max (aref distancias l i) (aref distancias l j)))

(defun group-average-linkage (i j l distancias)
  (* 0.5 (+ (aref distancias l i) (aref distancias l j))))

(defun median-linkage (i j l distancias)
  (+ (* 0.5 (aref distancias l i))
     (* 0.5 (aref distancias l j))
     (* -0.25 (aref distancias i j))))

(defparameter *linkage* #'single-linkage)

(defun seleccionar-linkage ()
  (format t "Seleccione un algoritmo de linkage:~%")
  (format t "1. Single linkage~%")
  (format t "2. Complete linkage~%")
  (format t "3. Group average linkage~%")
  (format t "4. Median average linkage~%")
  (let ((input (read)))
    (setf *linkage*
	  (cond ((eq 1 input) #'single-linkage)
		((eq 2 input) #'complete-linkage)
		((eq 3 input) #'group-average-linkage)
		((eq 4 input) #'median-linkage)
		(t (mostrar-seleccion-incorrecta #'seleccionar-linkage))))))

(defun linkage (i j l distancias)
  (funcall *linkage*  i j l distancias))


; ======== Representación dendrograma ========

(defstruct (nodo (:constructor crear-nodo)
		 (:conc-name nil))
  nivel hijo-i hijo-j datos)

(defstruct (dendrograma (:conc-name nil))
  nodo-superior)

(defun cortar-dendrograma (den nivel)
  "Obtiene los clusters cortando el dendrograma DEN por el nivel NIVEL"
  (loop for x in (cortar-dendrograma-aux (nodo-superior den) nivel) collect (aplanar-nodo x)))

(defun cortar-dendrograma-aux (nodo nivel)
  (cond ((datos nodo) (datos nodo))
	((> nivel (nivel nodo)) (append (cortar-dendrograma-aux (hijo-i nodo) nivel)
					(cortar-dendrograma-aux (hijo-j nodo) nivel)))
	(t (list nodo))))

(defun aplanar-nodo (nodo)
  "Obtiene todos los datos anidados en el nodo NODO"
  (cond ((null nodo) nil)
	((not (nodo-p nodo)) (list nodo))
	((datos nodo) (datos nodo))
	(t (append (aplanar-nodo (hijo-i nodo))
		   (aplanar-nodo (hijo-j nodo))))))

(defun crear-dendrograma (clusters)
  "Crea un dendrograma a partir de los clusters.
   Si CLUSTERS es una lista, se habrá usado el algoritmo de clustering jerarquico divisivo,
   siendo el primer cluster el nodo superior del dendrograma.
   Si CLUSTERS es un array, se habrá usado el algoritmo de clustering jerarquico aglomerativo,
   quedando un unico elemento no nulo en el array, que sea el nodo superior del dendrograma"
  (if (typep clusters 'list)
      (make-dendrograma :nodo-superior (first clusters))
      (make-dendrograma :nodo-superior (loop for x across clusters thereis x))))


; ======== Algoritmo de clustering jerarquico aglomerativo ========

;;; CLUSTERING JERARQUICO AGLOMERATIVO
;;; 1. Representar cada punto como un cluster
;;; 2. Calcular la matriz de proximidad
;;; 3. Mientras haya más de un cluster
;;;   3.1. Agrupar una pareja de clusters con la minima distancia
;;; 4. Generar los clusters cortanto del dendograma al nivel adecuado


(defun clustering-jerarquico-aglomerativo (datos num-clusters)
  (let ((clusters (crear-clusters datos))                            ;; 1.
	(matriz (crear-matriz-proximidad datos)))                    ;; 2.
    (loop for nivel from (1- (length datos)) downto 1 do             ;; 3.
	 (agrupa-clusters-distancia-minima clusters matriz nivel))   ;;   3.1.
    (cortar-dendrograma (crear-dendrograma clusters) num-clusters))) ;; 4.

(defun crear-clusters (datos)
  (let ((tam (length datos)))
    (make-array tam :initial-contents (loop for dato across datos collect
					   (crear-nodo :nivel tam :datos (list dato))))))

(defun crear-matriz-proximidad (datos)
  (let* ((n (length datos))
	 (matriz (make-array (list n n) :initial-element nil)))
    (loop for dato1 across datos for i below n do
	 (loop for dato2 across datos for j below i do
	      (let ((d (distancia dato1 dato2)))
		(setf (aref matriz i j) d
		      (aref matriz j i) d))))
    matriz))

(defun agrupa-clusters-distancia-minima (clusters matriz nivel)
  (let ((minimos (minimos-matriz matriz)))
    (agrupa clusters (first minimos) (second minimos) nivel)
    (actualizar-matriz matriz minimos)))

(defun agrupa (clusters i j nivel)
  (setf (svref clusters i) (crear-nodo :nivel nivel
				       :hijo-i (svref clusters i)
				       :hijo-j (svref clusters j))
	(svref clusters j) nil))

(defun minimos-matriz (matriz)
  (let ((n (array-dimension matriz 0)))
    (cdr (reduce #'min-cons (loop for x below n append (loop for y below x collect (list x y)))
		 :key (lambda (x) (cons (apply #'aref (cons matriz x)) x))))))

(defun actualizar-matriz (matriz minimos)
  (let ((n (array-dimension matriz 0))
	(i (first minimos))
	(j (second minimos)))
    (loop for l below n when (and (/= i l) (/= j l)) do
	 (when (and (aref matriz l i) (aref matriz l j)) ; Fila L no vacia
	   (let ((nueva-distancia (linkage i j l matriz)))
	       (setf (aref matriz l i) nueva-distancia
		     (aref matriz i l) nueva-distancia))))
    (eliminar-fila-columna j n matriz)))

(defun eliminar-fila-columna (j n matriz)
  (loop for j-i below n do (setf (aref matriz j j-i) nil
				 (aref matriz j-i j) nil)))


; ======== Algoritmo de clustering jerarquico divisivo ========

;;; Algoritmo DIANA
;;; 1. Representar todos los puntos como un cluster
;;; 2. Elegir un cluster Cl con el maximo diametro para dividirlo
;;; 3. Inicializar los clusters Ci = Cl, Cj = () en los que se va a dividir Cl
;;; 4. Calcular el elemento X-M de Ci con mayor diferencia de distancia media a Ci y a Cj
;;;   4.1. Si la diferencia es mayor que 0, mover el punto X-M de Ci a Cj y repetir los pasos 2. y 3.
;;;   4.2. En caso contrario, parar
;;; 5. Generar los clusters cortanto del dendograma al nivel adecuado

;;; NOTA: Por motivos de implementacion, despues de parar en el paso 4.2. es necesario añadir
;;; los nodos creados en la división al dendrograma y a la lista de CLUSTERS.
;;; Esto se realiza con la llamada a la funcion CREAR-NODOS


(defun algoritmo-diana (datos num-clusters)
  (let ((tam (length datos))
	(clusters (list (crear-nodo :datos (loop for dato across datos collect dato))))) ;; 1.
    (loop for nivel from 1 to (1- tam) do
	 (let* ((cluster-l (cluster-mayor-diametro clusters))          ;; 2.
		(datos-i (datos cluster-l))
		(datos-j '()))                                         ;; 3.
	   (loop while T do
		(let ((max-val (mayor-elemento-diferencia-distancias-medias datos-i datos-j))) ;; 4.
		  (if max-val (setf datos-i (remove max-val datos-i :count 1)
				    datos-j (push max-val datos-j))    ;; 4.1.
		      (return))))                                      ;; 4.2.
	   (crear-nodos cluster-l datos-i datos-j clusters nivel)))
    (cortar-dendrograma (crear-dendrograma clusters) num-clusters)))

(defun crear-nodos (cluster-l datos-i datos-j clusters nivel)
  (let ((hijo-i (crear-nodo :datos datos-i))
	(hijo-j (crear-nodo :datos datos-j)))
    (setf (nivel cluster-l) nivel
	  (datos cluster-l) nil
	  (hijo-i cluster-l) hijo-i
	  (hijo-j cluster-l) hijo-j)
    (nconc clusters (list hijo-i hijo-j))))

(defun cluster-mayor-diametro (clusters)
  (let ((cluster-max nil)
	(mayor-diametro nil))
    (loop for cluster in clusters when (datos cluster) do
	 (let ((diametro (calcular-diametro cluster)))
	   (when (or (not cluster-max) (> diametro mayor-diametro))
	     (setf cluster-max cluster
		   mayor-diametro diametro))))
    cluster-max))

(defun calcular-diametro (cluster)
  (loop for dato1 in (datos cluster) maximizing
       (loop for dato2 in (datos cluster) when (not (equal dato1 dato2)) maximizing
	    (distancia dato1 dato2))))


;;; Las formulas que aparecen en "Clustering / Rui Xu, Donald C. Wunsch" (pag. 38)
;;; distinguen el cálculo de la diferencia de la distancia media para la primera iteración
;;; del cálculo para las restantes iteraciones.

;;; Sin embargo, la primera iteración puede considerarse un caso especial en el que la
;;; distancia media de X-M con Cj es 0, al no haber ningun elemento, y obtenerse la misma fórmula
;;; De este modo, simplificamos el algortimo y reducimos el número de funciones necesarias.

(defun mayor-elemento-diferencia-distancias-medias (datos-i datos-j)
  (let ((max-elem (reduce #'max-cons datos-i :key (lambda (x) (diferencia-distancias-medias x datos-i datos-j)))))
    (if (> (car max-elem) 0) (cdr max-elem))))

(defun diferencia-distancias-medias (x-m datos-i datos-j)
  (let ((distancia-media (distancia-media x-m datos-i))
	(tam-datos-j (length datos-j)))
    (if (zerop tam-datos-j)
	distancia-media
	(cons (- (car (distancia-media x-m datos-i))
		 (/ (loop for x-q in datos-j summing (distancia x-m x-q)) tam-datos-j))
	      x-m))))

(defun distancia-media (x-m datos-cluster)
  (if (= 1 (length datos-cluster))
      (cons 0 x-m)
      (cons (/ (loop for x-p in datos-cluster when (not (equal x-m x-p)) summing (distancia x-m x-p))
	       (1- (length datos-cluster)))
	    x-m)))


;;; ======== Ejemplo con iris.lisp ========

(defparameter *iris* nil)
(load "iris.lisp")
(defparameter *datos-iris* (make-array (length *iris*) :initial-contents (loop for dato in *iris* collect (butlast dato))))

(defun ejecutar-iris-aglomerativo (num-clusters)
  "Ejecuta el algoritmo de clustering jerarquico aglomerativo para los datos de *IRIS*"
  (seleccionar-distancia)
  (seleccionar-linkage)
  (time (clustering-jerarquico-aglomerativo *datos-iris* num-clusters)))

(defun ejecutar-iris-divisivo (num-clusters)
  "Ejecuta el algoritmo DIANA para los datos de *IRIS*"
  (seleccionar-distancia)
  (time (algoritmo-diana *datos-iris* num-clusters)))

(defun tipo-dato-iris (dato)
  "Devuelve el tipo real de DATO en los ejemplos de *IRIS*"
  (loop for dato-iris in *iris*
     when (equal dato (butlast dato-iris))
     return (car (last dato-iris))))

(defun comprobar-iris (funcion num-clusters)
  "Muestra por pantalla los tipos segun cluster"
  (let ((clusters (funcall funcion *datos-iris* num-clusters)))
    (loop for cluster in clusters for num below num-clusters do
	 (loop for dato in cluster for tipo = (tipo-dato-iris dato)
	    counting (equal tipo 'Iris-setosa) into t1
	    counting (equal tipo 'Iris-versicolor) into t2
	    counting (equal tipo 'Iris-virginica) into t3
	    finally (format t "~&Cluster ~a~%  Setosa: ~a~%  Versicolor: ~a~%  Virginica: ~a~%~%" num t1 t2 t3)))))

(defun comprobar-iris-aglomerativo (num-clusters)
  (seleccionar-distancia)
  (seleccionar-linkage)
  (comprobar-iris #'clustering-jerarquico-aglomerativo num-clusters))

(defun comprobar-iris-divisivo (num-clusters)
  (seleccionar-distancia)
  (comprobar-iris #'algoritmo-diana num-clusters))


;;; ======== Ejemplo generado aleatoriamente ========

(defun generar-datos-aleatorios (num-ejemplos longitud-ejemplos valor-maximo)
  (make-array num-ejemplos
	      :initial-contents (loop repeat num-ejemplos collect (loop repeat longitud-ejemplos collect (random valor-maximo)))))

(defun ejecutar-aleatorio (num-ejemplos longitud-ejemplos valor-maximo num-clusters)
  (let ((datos-aleatorios (generar-datos-aleatorios num-ejemplos longitud-ejemplos valor-maximo)))
    (seleccionar-distancia)
    (seleccionar-linkage)
    (format t "~%Inicio de ejecución para CLUSTERING-JERARQUICO-AGLOMERATIVO~%")
    (time (clustering-jerarquico-aglomerativo datos-aleatorios num-clusters))
    (format t "Fin de ejecución para CLUSTERING-JERARQUICO-AGLOMERATIVO~%")
    (format t "~%Inicio de ejecución para ALGORITMO-DIANA~%")
    (time (algoritmo-diana datos-aleatorios num-clusters))
    (format t "Fin de ejecución para ALGORITMO-DIANA~%")))
